import * as monaco from "./editor";

const model = monaco.editor.createModel(
  `console.log('hello, world!')`,
  undefined,
  monaco.Uri.parse("file:///demo.ts")
);

monaco.editor.create(document.getElementById("demo"), {
  model,
});
