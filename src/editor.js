import EditorWorker from "monaco-editor-core/esm/vs/editor/editor.worker?worker";

// 注册 TypeScript 语言服务，提供代码补全、查找引用、重命名等语言能力
import "monaco-typescript/release/esm/monaco.contribution";
import TSWorker from "monaco-typescript/release/esm/ts.worker?worker";

// 注册 TypeScript 语言的语法解释器，提供语法高亮等，是 TypeScript 语言的基础能力
import "monaco-languages/release/esm/typescript/typescript.contribution";

// 构建 Monaco Editor 的运行环境，因为 WebWorker 必须使用独立文件的方式引入，所以 Monaco Editor 使
// 用了一个相对原始的方式引入其 worker ，注意需要使用 ?worker 来引入 Web Worker 。
self.MonacoEnvironment = {
  getWorker(_, label) {
    if (label === "typescript") {
      return new TSWorker();
    }
    return new EditorWorker();
  },
};

export * from "monaco-editor-core";
