# vite-monaco-editor

按需引入 Monaco Editor 的一个例子，仅引入了 TypeScript 语言服务。如果需要引入其它语言，不涉及 Web Worker 的，直接引入 `monaco-languages/release/esm/LANG/LANG.contribution` 即可。详情见代码注释。

注意：由于 Firefox 目前不支持 ESM 方式引入 WebWorker ，故开发模式只支持 Chrome 内核的浏览器。编译模式可正常支持 Firefox 。
